### Github Action CI-CD Normal Template


```yml
name: Laravel

on:
  push:
    branches: [master] // branch name
  pull_request:
    branches: [master]   // branch name

jobs:
  laravel-tests:
    runs-on: ubuntu-latest

    steps:
      - name: Checkout

        uses: actions/checkout@v2
        with:
          path: master
      - name: Deployment

        uses: appleboy/ssh-action@master

        with:
          host: ${{ secrets.SSH_HOST }}

          username: ${{ secrets.SSH_USERNAME }}

          key: ${{ secrets.SSH_PRIVATE_KEY }}

          script: |
            cd /var/www/html/sandip
            git pull
```

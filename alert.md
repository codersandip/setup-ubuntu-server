```js
function alertmsg(status,msg){
		const Toast = Swal.mixin({
			toast: true,
			position: 'top-end',
			showConfirmButton: false,
			timer: 2000,
			// timerProgressBar: true,
			onOpen: (toast) => {
				toast.addEventListener('mouseenter', Swal.stopTimer)
				toast.addEventListener('mouseleave', Swal.resumeTimer)
			}
		})
		Toast.fire({
			icon: status,
			title: msg
		})
	}
$('#msform').submit(function(e){
			e.preventDefault();        
			var form = $(this);
			var data = new FormData();
			var checkboxArray = [];
			$(this).find('textarea, input, select').each(function(){
				var tagName = $(this)[0].tagName;
				var tagType = $(this).attr('type')? $(this).attr('type').toUpperCase() : '';
				if(tagName!='INPUT' || (tagName=='INPUT' && tagType!='FILE')){
					var name = $(this).attr('name');
					var val = $(this).val();
					if(tagType == 'RADIO'){
					if($(this).prop('checked')){
						data.append(name, val);
					}
					} else if (tagType == 'CHECKBOX') {
					if($(this).prop('checked')){
						var checkboxval = checkboxArray[name];
						if(checkboxval == null) {
						checkboxArray[name] = [val];
						} else {
						checkboxArray[name].push(val);
						}            
					}
					} else {
					data.append(name, val);
					}        
				}
			});
			for(var k in checkboxArray) {
				data.append(k, checkboxArray[k]);
			}
			// $(this).find('input[type="file"]').each(function(){
			//     if($(this)[0].files.length>0){
			//         data.append($(this).attr('name'), $(this)[0].files[0]);
			//     }
			// });
			console.log(data)
			
			$.ajax({
				url: '/api/admin/customerdetails',
				data: data,
				contentType:"application/json",
				cache: false,
				contentType: false,
				processData: false,
				method: 'POST',
				beforeSend: function() {
				},
				success: function(data){
					console.log(data)
					if( data.status == 200) {
						Swal.fire('Thanks!',data.msg,'success');
						localStorage.setItem('customerid',data.data.id);
						setTimeout(() => {
							window.location = "/customer/packages";
						}, 1000);
					}
					else{
						// alert(data.msg);
						$('#stepperform').modal('close');
						Swal.fire('Thanks',data.msg,'success');
					}
				},
				error: function(data){
					console.log(data);
					Swal.fire('Error',data.responseJSON.msg,'error');
				}
			});
		});
``
